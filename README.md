# CS-E4300: Week 1
Adriaan Knapen - adriaan.knapen@aalto.fi - 727684

## Server

We start of with a clean CentOS 7.5 server at Digital Ocean and access it using SSH. Appendix 1 contains the commands use to setup the server. Server sided certificates are generated as shown in Appendix 2.

Appendix 3 covers how to setup the webserver. The webserver consists of an NGINX server serving static html files for each of the _things_ which can be queried. It is configured such that it serves its own certificate to the client and checks if the client request included a valid client certificate.

## Client

We start of by generating a client certificate at our CA by executing the following commands in Appendix 4 at our server.

At this point we import `ca.cert` as a root certificate and `client.p12` as personal certificate to our browser - checked with Firefox - and we can access our web page using HTTPS with a valid server and client certificate. If we would not have imported our client certificate, then the server would have bounced our request since the client certificate verification would not have succeeded.

Clone the [git](https://gitlab.com/Addono/cs-e4300-week-1) repository to the client machine and copy the `ca.crt`, `client.crt`, `client.key` from the server to the `client` folder in the repository. Then open a shell and point it at the `client` folder. Run the following command:

```bash
connect.py --host HOSTNAME -p PASSWORD -t THING_TO_QUERY
```

The client uses the `ssl` library from Python to wrap TLS around a `socket`. The `ssl` library is instructed to both check the server certificate `ssl.create_default_context(ssl.Purpose.SERVER_AUTH, cafile=server_cert)`and supply the client certificate `ssl.SSLContext#load_cert_chain(certfile=client_cert, keyfile=client_key, password=client_password)`when connecting to the server.

### Security

Currently does the client require the password to be given as a command line argument, which makes that the password can potentially end up in logs, thus causing a security related issue.

Another minor issue is that the client does not use input validation on the queried _thing_ when creating the HTTP request, hence if a malicious user could control the _thing_ to be queried then e.g. unintended headers could be added to the HTTP request.

## Appendixes
### Appendix 1
```bash
openssl req -new -x509 -keyout ca.key -out ca.crt -config ./source/server/openssl.cnf # Generate the root CA
openssl genrsa -aes256 -out server.key 1024 # Generate keypair for the CA
openssl req -new -key server.key -out server.csr -config ./source/server/openssl.cnf # Generate a Certificate Signing Request for the server
openssl ca -in server.csr -out server.crt -cert ca.crt -keyfile ca.key -config # Create the server's certificate, when asked for the common name enter the server's domain name
openssl ca -name CA_default -gencrl -keyfile ca.key -cert ca.crt -out ca.crl -config openssl.cnf -crldays 1095 # Create the client certificate used by the server
```

### Appendix 2
```bash
sudo yum install epel-release nginx firewalld git wget -y

sudo systemctl start nginx firewalld
sudo systemctl enable nginx firewalld

sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --reload

cd ~ # Move to the home directory of our user, from here on will we asume we are the root user
git clone https://gitlab.com/Addono/cs-e4300-week-1.git ./source # Retrieve server config files
touch index.txt & echo "01" > crlnumber & echo "1000" > serial # Create the index, crlnumber and serial file
mkdir newcerts crl certs # Create the required directories

reboot # Ensure that all changes take effect.
```
### Appendix 3
Setup NGINX with the source files and install the certificate.

```bash
rm /etc/nginx/nginx.conf -f; cp ~/source/server/nginx.conf /etc/nginx/ # Overwrite the NGINX config
cd /usr/share/nginx
cp -r ~/source/server/nginx/* ./html/ # Copy the server files to nginx
mkdir cert # Create a directory to store the certificates in
cp ~/server.crt ./cert/; cp ~/ca.crt ./cert/; cp ~/ca.crl ./cert/; cp ~/server.key ./cert/ # Copy the server's certificates and key
echo "secret" > ./cert/global.pass # Store the password of the server key
systemctl restart nginx # Restart NGINX for the changes to take effect
```
### Appendix 4
```bash
openssl genrsa -aes256 -out client.key 1024 # Create a new keypair
openssl req -new -key client.key -out client.csr # Create a certificate request
openssl x509 -req -days 1095 -in client.csr -CA ca.crt -CAkey ca.key -CAserial serial -CAcreateserial -out client.crt -config openssl.cnf # Create the certificate
openssl pkcs12 -export -clcerts -in client.crt -inkey client.key -out client.p12 # Optional
```