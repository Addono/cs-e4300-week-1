import os
import re
import socket
import ssl

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-t", "--thing", dest="thing", help="The thing to request", metavar="THING", required=True)
parser.add_argument("--host", dest="host", help="The hostname of the server to contact", metavar="HOSTNAME",
                    required=True)
parser.add_argument("--server-cert", dest="server_cert", help="The path to the CA certificate of the server",
                    metavar="PATH", default="./ca.crt")
parser.add_argument("--client-cert", dest="client_cert", help="The path to the certificate of the client",
                    metavar="PATH", default="./client.crt")
parser.add_argument("--client-key", dest="client_key", help="The path to the key of the client", metavar="PATH",
                    default="./client.key")
parser.add_argument("-p", "--client-password", dest="client_password", help="The password of the client key",
                    metavar="PASSWORD")
parser.add_argument("-v", dest="verbose", help="Use to get a more verbose output", action="store_true", default=False)

args = parser.parse_args()

requested_thing = args.thing

host_addr = args.host
host_port = 443

server_cert = os.path.abspath(args.server_cert)
client_cert = os.path.abspath(args.client_cert)
client_key = os.path.abspath(args.client_key)

client_password = args.client_password
verbose = args.verbose

context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH, cafile=server_cert)
if verbose: print("> Loading certificate chain")
context.load_cert_chain(certfile=client_cert, keyfile=client_key, password=client_password)
if verbose: print("> Certificate chain loaded")

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
conn = context.wrap_socket(s, server_side=False, server_hostname=host_addr)
conn.connect((host_addr, host_port))
if verbose: print("> SSL established. Peer: {}".format(conn.getpeercert()))
if verbose: print("> Sending HTTP GET request")
conn.send(str.encode("GET /" + requested_thing + "/ HTTP/1.1\r\nHost: " + host_addr + "\r\n\r\n"))
if verbose: print("> Waiting for the webserver to return")
output = conn.read()
decoded_output = output.decode("utf-8")
match = re.search(r'\n\r\n(\w*)\n', decoded_output)
if match is not None:
    print("< Detected body value: " + match.group(1))
else:
    print("> ERROR: Could not find the body of the request")

if verbose: print("< Dumping full output:\n" + decoded_output)

if verbose: print("> Closing connection")
conn.close()
